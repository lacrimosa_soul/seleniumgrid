using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using System;


namespace Tests
{
    public class Tests
    {

        string HubUrl = "http://192.168.100.84:4444/wd/hub";
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestChrome()
        {
            ChromeOptions Options = new ChromeOptions();
            IWebDriver driver = new RemoteWebDriver(new Uri(HubUrl), Options);
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("http://www.github.com");
            driver.Quit();
        }

        [Test]
        public void TestFirefox()
        {
            FirefoxOptions Options = new FirefoxOptions();
            IWebDriver driver = new RemoteWebDriver(new Uri(HubUrl), Options);
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("http://www.github.com");
            driver.Quit();
        }
    }
}